# Developer Interface/Console – jseoqdeveloperinterface

The _jseoqdeveloperinterface_ is a Website that provides manual access to an EOQ webserver through a web socket. The developer interface allows to specify EOQ commands manually and see the results. Moreover, it will list any EOQ event. 

To start the developer interface open jseoqdeveloperinterface.html in your browser. 
By default, the developer interface to connect to 

ws://localhost:8000/ws/eoq.do. 

If that was changed that must be changed in the source code.

The developer interface works out-of-the-box with the [Python EOQ Web Server](https://gitlab.com/eoq/tools/pyeoqwebserver/). The EOQ Web Server has to be started before
the jseoqdeveloperinterface is opened. 

The jseoqdeveloperinterface is based on [JsEOQ](https://gitlab.com/eoq/js/jseoq).


# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 

