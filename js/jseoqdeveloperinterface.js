var domain = null; //the eoq domain
		
//UI elements
var statusAlertPane = $('#status-alert-pane');
var uiPane = $('#ui-pane');
var logPane = $('#console-output-pane');


function ConnectToDomain(url){
	return new Promise(function(resolve,reject) {
		let logger = new eoq2.util.ConsoleLogger(eoq2.util.DEFAULT_LOG_LEVELS.concat([eoq2.util.LogLevels.DEBUG]));
		let domain = new eoq2.domain.remote.WebSocketDomain(url,0,logger);
		domain.Open().then(function(val) {
			legacyDomain = new eoq2.legacy.Jseoq1LegacyDomain(domain);
			resolve(domain);
		}).catch(function(e) {
			reject(e); //exception forwarding
		});
	});
}

function InitConnection(domain,user,password,successCallback,failedCallback) {
	//let cmd = new eoq2.Get(new eoq2.Qry().Met('CLASS').Pth('name'));
	let cmd = new CMD.Hel(user,password);
	domain.RawDo(cmd).then(
			function(res) {
				try {
					let val = eoq2.ResGetValue(res);
					ConnectionSuccess(domain.url,"sessionId: "+val);
					successCallback();
				} catch (e) {
					ConnectionFailed(domain.url,e.toString());
					failedCallback();
				}
			}
	);
}

function ConnectionSuccess(address,name) {
	statusAlertPane.html('<div class="alert alert-success alert-dismissible fade show" role="alert">' +
						   '	<strong>Connected!</strong> You are connected to domain '+ name+' @ '+address+'.' +
						   '  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
						   '	<span aria-hidden="true">&times;</span>' +
						   '  </button>' +
						   '</div>');
}

function ConnectionFailed(address,reason) {
	statusAlertPane.html('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
						   '	<strong>Connection failed!</strong> Could not connect to '+address+'. Reason: '+reason +
						   '  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
						   '	<span aria-hidden="true">&times;</span>' +
						   '  </button>' +
						   '</div>');
}

function EnableUi(enable) {
	if(enable) {
		uiPane.removeClass('d-none');
	} else {
		uiPane.addClass('d-none');
	}
}

function InitializeEoqDeveloperInterface(url,user,password) {
	EnableUi(false);
	ConnectToDomain(url).then(function(val) {
		domain = val;
		InitConnection(domain,user,password,
			function() {
			 EnableUi(true);
			 domain.Observe(OnEvents,['CHG','OUP','INP','CST','CVA','MSG'/*,'CUS'*/]); //listen to all kinds of events except custom events
			},
			function() {
			 EnableUi(false);
			});
	}).catch(function(e) {
		ConnectionFailed(url,e.toString());
		EnableUi(false);
	});
};
	
function ExecCommand(commandStr) {
	var commandInfo = {
		success : false,
		commandStr : commandStr,
		startTime : new Date(),
		resultStr : 'JSEQO UNKNOWN INTERNAL ERROR',
		endTime : 0
	}
	try {
		//let cmd = eval(commandStr);
		let cmd = textSerializer.Des(commandStr);
		let textCmd = textSerializer.Ser(cmd);
		commandInfo.commandStr = textCmd;
		domain.RawDo(cmd).then(function(res) {
			commandInfo.endTime = new Date();
			try {
				let textRes = textSerializer.Ser(res);
				commandInfo.resultStr = textRes;
				let val = eoq2.ResGetValue(res);
				commandInfo.success = true;
				ShowResult(commandInfo);
			} catch(e) {
				commandInfo.endTime = new Date();
				commandInfo.success = false;
				commandInfo.resultStr = ExceptionToString(e);
				ShowResult(commandInfo);
			}
			
		});
	} catch (e) {
		commandInfo.endTime = new Date();
		commandInfo.success = false;
		commandInfo.resultStr = ExceptionToString(e);
		ShowResult(commandInfo);
	}
};

function ExecLegacyCommand(commandStr) {
	var commandInfo = {
		success : false,
		commandStr : commandStr,
		startTime : new Date(),
		resultStr : 'JSEQO UNKNOWN INTERNAL ERROR',
		endTime : 0
	}
	try {
		let cmd = jseoq.CommandParser.StringToCommand(commandStr);
		legacyDomain.DoSync(cmd).then(function(result) {
			commandInfo.endTime = new Date();
			
			try {
				commandInfo.resultStr = jseoq.ResultParser.ResultToString(result);
		
				if(jseoq.ResultParser.IsResultOk(result)) {
					commandInfo.success = true;
				}
				
				ShowResult(commandInfo);
			} catch(e) {
				commandInfo.endTime = new Date();
				commandInfo.success = false;
				commandInfo.resultStr = ExceptionToString(e);
				ShowResult(commandInfo);
			}
			
		});
	} catch (e) {
		commandInfo.endTime = new Date();
		commandInfo.success = false;
		commandInfo.resultStr = ExceptionToString(e);
		ShowResult(commandInfo);
	}
};

function ExceptionToString(e) {
	exceptionStr = e.message + '<br/>' +
				   e.fileName + '(line: ' + e.lineNumber + ', col:' + e.columnNumber + ')<br/>' +
				   e.stack;
	
	return exceptionStr;
};

function ShowResult(commandInfo) {
	var duration = commandInfo.endTime - commandInfo.startTime;
	
	var date = commandInfo.endTime.getFullYear()+'-'+(commandInfo.endTime.getMonth()+1)+'-'+commandInfo.endTime.getDate();
	var time = commandInfo.endTime.getHours() + ":" + commandInfo.endTime.getMinutes() + ":" + commandInfo.endTime.getSeconds();
	var duration = commandInfo.endTime - commandInfo.startTime;
	var style = 'result-ok';
	if(!commandInfo.success) {
		style = 'result-error';
	}
	logPane.prepend('<div class="alert" role="alert">' +
				   '	<small>'+date+' '+time+' @ '+duration+' ms: (<a href="#" onclick="ExecCommand(\''+commandInfo.commandStr+'\');">repeat</a>) (<a href="#" onclick="commandArea.val(\''+commandInfo.commandStr+'\');">copy to console</a>)</small>' +
				   '    <div class="command">'+commandInfo.commandStr+'</div>'+
				   '    <div class="'+style+'">'+ commandInfo.resultStr + '</div>'+
				   '</div>');
};

function OnEvents(evts,source) {
	for(let i=0;i<evts.length;i++) {
		evt = evts[i];
		evtStr = JSON.stringify(evt);
		ShowEvent(evtStr);
	}
}

function ShowEvent(evtStr) {
	let endTime = new Date();
	let date = endTime.getFullYear()+'-'+(endTime.getMonth()+1)+'-'+endTime.getDate();
	let time = endTime.getHours() + ":" + endTime.getMinutes() + ":" + endTime.getSeconds();
	let style = 'event';
	logPane.prepend('<div class="alert" role="alert">' +
				   '	<small>'+date+' '+time+' </small>' +
				   '    <div class="'+style+'">'+ evtStr + '</div>'+
				   '</div>');
};

function ClearLogPane() {
	logPane.html('');
};


//AUTO STARTUP of user interface
var url = 'ws://localhost:8000/ws/eoq.do';
var user = 'admin'; //bogus: is not checked so far
var password = '0000'; //bogus: is not checked so far
var domain = null;
var legacyDomain = null;
var textSerializer = new eoq2.serialization.TextSerializer();

InitializeEoqDeveloperInterface(url,user,password);